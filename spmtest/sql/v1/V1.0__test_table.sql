CREATE SCHEMA IF NOT EXISTS spm_test;

------------------------------------------------------------------------------------
-- Test table
------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS spm_test.test_table (
    row_id                 UUID              NOT NULL,
    some_time              TIMESTAMPTZ       NOT NULL,
    some_id                INT               NOT NULL
)
PARTITION BY RANGE (some_time);

CREATE INDEX spm_test_test_table_some_id_idx ON spm_test.test_table(some_id);
select spm.initialize_partitioned_table('spm_test.test_table', '1 minute', '30 minutes');

------------------------------------------------------------------------------------
-- Test agg table
------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS spm_test.test_agg_table (
    some_time_bucket       TIMESTAMPTZ       NOT NULL,
    some_count             INT               NOT NULL
)
PARTITION BY RANGE (some_time_bucket);
select spm.initialize_partitioned_table('spm_test.test_agg_table', '10 minutes', '30 minutes');

------------------------------------------------------------------------------------
-- Test agg function
------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION spm_test.test_agg(
	from_time timestamp with time zone,
	to_time timestamp with time zone)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
  v_temprow record;
BEGIN
    insert into spm_test.test_agg_table
        select date_bin('2 minutes', some_time, to_timestamp(0)) as some_time_bucket, count(*)
        from spm_test.test_table
        where some_time>=from_time and some_time<to_time
        group by some_time_bucket;
END;
$BODY$;

select spm.create_agg_job('spm_test.test_agg', '5 minutes'::interval, now()-'30 minutes'::interval);