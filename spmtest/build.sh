#!/bin/bash
set -e

APP_NAME=localhost:5000/spmtest_dbmigrate
APP_VERSION=0.0.1

docker build -t $APP_NAME:$APP_VERSION .