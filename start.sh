#!/bin/bash
set -e
(cd dbmigrate && ./build.sh)
(cd spmtest && ./build.sh)

source .env

db_volume_name=compose_postgres

init_stuff() {
    echo "Initing stuff"
    docker compose up -d db
    sleep 10
    docker compose run --rm spm_dbmigrate
    docker compose run --rm spmtest_dbmigrate
}

update_stuff() {
    echo "Updating stuff"
    docker compose up -d db
    sleep 10
    docker compose run --rm spm_dbmigrate
    docker compose run --rm spmtest_dbmigrate
}

db_hit=$(docker volume ls -q -f name=$db_volume_name)
if [[ -z "$db_hit" ]]; then
   init_stuff
else
    update_stuff
fi

docker compose up -d
