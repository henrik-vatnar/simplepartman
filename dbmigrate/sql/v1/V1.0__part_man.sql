CREATE SCHEMA IF NOT EXISTS spm;

-------------------------------------------------------
--table holding info about partitioned tables
-------------------------------------------------------
CREATE TABLE IF NOT EXISTS spm.partition_info ( 
 table_name text,
 range_size interval not null,
 retention_time interval not null,
 last_maintenance_run timestamptz
);

-------------------------------------------------------
--function for creating partitions
-------------------------------------------------------
CREATE OR REPLACE FUNCTION spm.create_partitions(table_name text, from_time timestamptz, to_time timestamptz, part_size interval)
    RETURNS void
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_temprow record;
  v_sql_statement text;
BEGIN
    FOR v_temprow in 
      with existing_partitions as (
        select pt.relname as partition_name,
             (regexp_matches(pg_get_expr(pt.relpartbound, pt.oid, true), '\((.+?)\).+\((.+?)\)'))[1]::timestamptz as range_from,
             (regexp_matches(pg_get_expr(pt.relpartbound, pt.oid, true), '\((.+?)\).+\((.+?)\)'))[2]::timestamptz as range_to
        from pg_class base_tb 
          join pg_inherits i on i.inhparent = base_tb.oid 
          join pg_class pt on pt.oid = i.inhrelid
        where base_tb.oid = table_name::regclass
      ),
      candidate_partitions as (
        select generate_series(from_time, to_time, part_size) as range_from
      )
      select distinct cp.range_from
      from candidate_partitions cp
      where cp.range_from not in (select range_from from existing_partitions)
      order by cp.range_from asc
    LOOP
        v_sql_statement := 'CREATE TABLE %1$s_%2s PARTITION OF %1$s FOR VALUES FROM (%3$L) TO (%4$L)';
        v_sql_statement := FORMAT(v_sql_statement, table_name, to_char(v_temprow.range_from, 'YYYYMMDDHH24MISS'), v_temprow.range_from, v_temprow.range_from+part_size);
        raise notice '%', v_sql_statement;
		EXECUTE v_sql_statement;
    END LOOP;
END;
$$;

-------------------------------------------------------
--function for dropping partitions
-------------------------------------------------------
CREATE OR REPLACE FUNCTION spm.drop_partitions(table_name text, older_than timestamptz)
    RETURNS void
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_temprow record;
  v_sql_statement text;
BEGIN
    FOR v_temprow in 
      with existing_partitions as (
        select pt.relnamespace::regnamespace::text||'.'||pt.relname as partition_name,
             (regexp_matches(pg_get_expr(pt.relpartbound, pt.oid, true), '\((.+?)\).+\((.+?)\)'))[1]::timestamptz as range_from,
             (regexp_matches(pg_get_expr(pt.relpartbound, pt.oid, true), '\((.+?)\).+\((.+?)\)'))[2]::timestamptz as range_to
        from pg_class base_tb 
          join pg_inherits i on i.inhparent = base_tb.oid 
          join pg_class pt on pt.oid = i.inhrelid
        where base_tb.oid = table_name::regclass
      )
      select partition_name, range_to
      from existing_partitions
	  where range_to < older_than
    LOOP
        v_sql_statement := 'DROP TABLE %1$s';
        v_sql_statement := FORMAT(v_sql_statement, v_temprow.partition_name);
        raise notice '%', v_sql_statement;
		EXECUTE v_sql_statement;
    END LOOP;
END;
$$;

-------------------------------------------------------
--function for initializing range based partitioned table
-------------------------------------------------------
CREATE OR REPLACE FUNCTION spm.initialize_partitioned_table(part_table_name text, partition_range_size interval, retention_time interval)
    RETURNS void
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_sql_statement text;
BEGIN
	INSERT INTO spm.partition_info(table_name, range_size, retention_time, last_maintenance_run)
	VALUES(part_table_name, partition_range_size, retention_time, null);
	v_sql_statement := 'CREATE TABLE %1$s_default PARTITION OF %1$s DEFAULT';
    v_sql_statement := FORMAT(v_sql_statement, part_table_name);
	EXECUTE v_sql_statement;
	PERFORM spm.do_partition_maintenance(part_table_name);
END;
$$;

-------------------------------------------------------
--function for setting retention
-------------------------------------------------------
CREATE OR REPLACE FUNCTION spm.set_partition_retention(part_table_name text, part_retention_time interval)
    RETURNS void
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_sql_statement text;
BEGIN
	UPDATE spm.partition_info SET retention_time=part_retention_time WHERE table_name=part_table_name;
	PERFORM spm.do_partition_maintenance(part_table_name);
END;
$$;

-------------------------------------------------------
--function for doing partition maintenance
-------------------------------------------------------
CREATE OR REPLACE FUNCTION spm.do_partition_maintenance(part_table_name text)
    RETURNS void
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_sql_statement text;
  v_temprow record;
  v_column_name text;
  v_retention_time interval;
  v_range_size interval;
BEGIN

	select retention_time, range_size INTO v_retention_time, v_range_size
	from spm.partition_info
	where table_name=part_table_name;
	
	select (regexp_matches(pg_get_partkeydef(c.oid), '\((.+?)\)'))[1] into v_column_name
	from   pg_class c
	where  c.relkind = 'p' and relnamespace::regnamespace::text||'.'||relname=part_table_name;
	
	--delete stuff from default which is outside range
	v_sql_statement := 'DELETE FROM %1$s_default WHERE %2$s<now()-%3$L::interval';
	v_sql_statement := FORMAT(v_sql_statement, part_table_name, v_column_name, v_retention_time);
	EXECUTE v_sql_statement;
	
	v_sql_statement := 'CREATE TEMPORARY TABLE part_maint AS SELECT * FROM %1$s_default;TRUNCATE %1$s_default';
	v_sql_statement := FORMAT(v_sql_statement, part_table_name);
	EXECUTE v_sql_statement;
	
	
	perform spm.create_partitions(part_table_name, date_bin(v_range_size, now()-v_retention_time, to_timestamp(0)), now()+(v_range_size*3), v_range_size);
	
	v_sql_statement := 'INSERT INTO %1$s SELECT * FROM part_maint';
	v_sql_statement := FORMAT(v_sql_statement, part_table_name);
	EXECUTE v_sql_statement;
	
	DROP TABLE part_maint;
	
	perform spm.drop_partitions(part_table_name, now()-v_retention_time);
	

END;
$$;

-------------------------------------------------------
--function for doing maintenance on all registered partitions
-------------------------------------------------------
CREATE OR REPLACE FUNCTION spm.do_partition_maintenance_all()
    RETURNS void
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_temprow record;
BEGIN
    FOR v_temprow in 
      select *
	    from spm.partition_info
    LOOP
      raise notice 'Doing partition maintenance for %', v_temprow.table_name;
      PERFORM spm.do_partition_maintenance(v_temprow.table_name);
    END LOOP;
END;
$$;
