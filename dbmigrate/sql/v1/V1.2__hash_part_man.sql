-------------------------------------------------------
--function for creating hash partitions
-------------------------------------------------------
CREATE OR REPLACE FUNCTION spm.create_hash_partitions(table_name text, number_of_partitions integer)
    RETURNS void
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_temprow record;
  v_sql_statement text;
BEGIN
    FOR v_temprow in 
    	select part_num
		from generate_series(0, number_of_partitions-1) as part_num
    LOOP
        v_sql_statement := 'CREATE TABLE %1$s_%2$s PARTITION OF %1$s FOR VALUES WITH (MODULUS %3$s, REMAINDER %2$s)';
        v_sql_statement := FORMAT(v_sql_statement, table_name, v_temprow.part_num, number_of_partitions);
        raise notice '%', v_sql_statement;
		EXECUTE v_sql_statement;
    END LOOP;
END;
$$;

