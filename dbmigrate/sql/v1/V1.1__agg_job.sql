-------------------------------------------------------
--table holding info about time series aggregation jobs
-------------------------------------------------------
CREATE TABLE IF NOT EXISTS spm.agg_job_info ( 
 function_name text not null,
 run_interval interval not null,
 last_end_time timestamptz
);

-------------------------------------------------------
--function for creating time series aggregation jobs
-------------------------------------------------------
CREATE OR REPLACE FUNCTION spm.create_agg_job(agg_function_name text, agg_interval interval, start_time timestamptz)
    RETURNS void
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_temprow record;
  v_sql_statement text;
BEGIN
	insert into spm.agg_job_info(function_name, run_interval, last_end_time ) 
		values(agg_function_name, agg_interval, date_bin(agg_interval, start_time, to_timestamp(0)));
END;
$$;

-------------------------------------------------------
--function for running aggregation jobs
-------------------------------------------------------
CREATE OR REPLACE PROCEDURE spm.run_agg_job(agg_function_name text, from_time timestamptz, to_time timestamptz, agg_interval interval)
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_temprow record;
  v_sql_statement text;
BEGIN
	FOR v_temprow in 
      select current_time_from
	  from generate_series(from_time, to_time, agg_interval) current_time_from
	  where current_time_from<=now()-agg_interval
    LOOP
        v_sql_statement := 'select %1$s(%2$L, %3$L)';
        v_sql_statement := FORMAT(v_sql_statement, agg_function_name, v_temprow.current_time_from, v_temprow.current_time_from+agg_interval);
        raise notice 'Current Time:% Statement:%', now(), v_sql_statement;
		EXECUTE v_sql_statement;
		update spm.agg_job_info set last_end_time=v_temprow.current_time_from+agg_interval where function_name=agg_function_name;
		commit;
    END LOOP;
END;
$$;

-------------------------------------------------------
--function for running all aggregation jobs
-------------------------------------------------------
CREATE OR REPLACE PROCEDURE spm.maintenance_agg_jobs()
    LANGUAGE PLPGSQL
    AS $$
DECLARE
  v_job_row record;
  v_sql_statement text;
BEGIN
	FOR v_job_row in
	  	select * from spm.agg_job_info
	  	where last_end_time+run_interval < now()
    LOOP
		call spm.run_agg_job(v_job_row.function_name, v_job_row.last_end_time, now(), v_job_row.run_interval);
    END LOOP;
END;
$$;