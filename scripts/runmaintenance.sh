#!/bin/bash
set -e

echo "Waiting 10 seconds on database initially"
sleep 10s
while :
do
    echo "Running part maintenance"
    time psql -c "select spm.do_partition_maintenance_all()"
    echo "Waiting 60 seconds"
    sleep 60s
done
popd

