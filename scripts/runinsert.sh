#!/bin/bash
set -e

echo "Waiting 60 seconds initially"
sleep 60s
while :
do
    echo "Running insert data"
    time psql -c "INSERT INTO spm_test.test_table select gen_random_uuid(), now(), (random() * 10)"
    echo "Waiting 1 second"
    sleep 1s
done
popd

