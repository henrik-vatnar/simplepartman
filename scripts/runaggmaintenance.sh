#!/bin/bash
set -e

echo "Waiting 60 seconds initially"
sleep 60s
while :
do
    echo "Running agg maintenance"
    time psql -c "call spm.maintenance_agg_jobs()"
    echo "Waiting 60 seconds"
    sleep 60s
done
popd

